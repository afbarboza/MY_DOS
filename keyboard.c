#include <keyboard.h>
#include <asm.h>

__asm__(".code16gcc\t\n");

/**
* read_keyboard - reads a keyboard using BIOS call
*/
char read_keyboard(void)
{
	asm(".code16gcc;\t\n");
	char read_key = 0;
	__asm__ __volatile__
	(
		"xorw	%%ax, %%ax;\t\n"
		"movb	$0x00, %%ah;\t\n"
		"int	$0x16;\t\n"
		"movb	$0x00, %%ah;\t\n"
		:"+a"(read_key)
		:
	);
	terminal_writechar(read_key);
	return read_key;
}
