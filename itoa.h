#ifndef	_ITOA_H_
#define	_ITOA_H_

#define	BIN_BASE	0	/*print in binary basis*/
#define	DEC_BASE	1	/*print in decimal basis*/
#define	HEX_BASE	2	/*print in hexadecimal basis*/


#define	BITS_PER_BYTE	8
#define	BITS_PER_NIBBLE	4

/**
* itoa - converts an given @n integer value
* to its ascii-string representation.
*
* @n: the value to be converted.
* @buffer: the outputed string.
* @radix: the base which will be printed.
*
* if @radix is equals to BIN_BASE, DEC_BASE or HEX_BASE
* then the string will receive the binary, decimal or
* hexadecimal representation, respectively.
*/
void itoa(int n, char buffer[], int radix);

#endif	 /*itoa.h*/
