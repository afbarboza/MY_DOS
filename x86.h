#ifndef _X86_H_
#define	_X86_H_

#include <stdint.h>

uint8_t inb(uint16_t port);

void outb(uint8_t value, uint16_t port);

uint16_t inw(uint16_t port);

void outw(uint16_t value, uint16_t port);

uint8_t bit_test(uint8_t bitbase, uint8_t offset);

uint8_t bit_set(uint8_t bitbase, uint8_t offset);

uint8_t bit_clear(uint8_t bitbase, uint8_t offset);

/*TODO: implement 32bit I/O ports*/

#endif /* x86.h */
