#include <x86.h>

uint8_t inb(uint16_t port)
{
	uint8_t value = 0;
	asm volatile("inb %1, %0" : "=a" (value) : "dN" (port));
	return value;
}

void outb(uint8_t value, uint16_t port)
{
	asm volatile("outb %0, %1" : : "a" (value), "dN" (port));
}

uint16_t inw(uint16_t port)
{
	uint16_t value = 0;
	asm volatile("inw %1, %0" : "=a"(value) : "dN"(port));
	return value;
}

void outw(uint16_t value, uint16_t port)
{
	asm volatile("outw %0, %1" : : "a"(value), "dN"(port));
}

/*TODO: implement 32bit I/O ports*/

