#ifndef	_DISK_H_
#define	_DISK_H_

#include <stdint.h>

uint32_t sectors_per_track = 0;

void lba2chs(uint32_t lba, uint8_t *head, uint8_t *track, uint8_t *sector);

void init_disk(void);

void *dread(uint32_t lba, unsigned size);

void dwrite(const void *buff, unsigned size);

#endif	/* disk.h */
