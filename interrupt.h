#ifndef _INTERRUPT_H_
#define	_INTERRUPT_H_

#include <asm.h>

#define	make_kernel_call(r)								\
	asm volatile("pusha;nop;");							\
	__asm__ __volatile__ (								\
		"nop;"									\
		"nop;"									\
		"int $0x21;"								\
		"nop;"									\
		"nop;"									\
		:									\
		:"a"(r.ax), "b"(r.bx), "c"(r.cx), "d"(r.dx), "S"(r.si), "D"(r.di)	\
	);										\
	asm volatile("nop; popa;")


#define	make_bios_call(entry_ivt, r)							\
	asm volatile("pusha;nop;");							\
	__asm__ __volatile__ (								\
		"nop;"									\
		"nop;"									\
		entry_ivt								\
		"nop;"									\
		"nop;"									\
		:"=a"(r.ax), "=b"(r.bx), "=c"(r.cx), "=d"(r.dx), "=S"(r.si), "=D"(r.di)	\
		:"a"(r.ax), "b"(r.bx), "c"(r.cx), "d"(r.dx), "S"(r.si), "D"(r.di)	\
	);										\
	asm volatile("nop; popa;")

#define	load_es(x) __asm__ __volatile__("movw %%ax, %%es;": :"a"(x))
#define	load_fs(x) __asm__ __volatile__("movw %0, %%fs;": :"r"(x))
#define load_gs(x) __asm__ __volatile__("movw %0, %%gs;": :"r"(x))


/**
 * handle_interrupt - handle a kernel syscall
 *
 */
volatile void handle_interrupt(void);

/**
 * replace_bios_interrupt - hook BIOS interrupts, installing
 *	kernel inteeupts
 *
 */
void replace_bios_interrupt(void);

#endif
