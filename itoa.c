#include <itoa.h>
#include <string.h>

__asm__(".code16gcc\t\n");

static void convert_to_bin_string(int input, char output[])
{
	char bit = 0;
	int i = (sizeof(int) * BITS_PER_BYTE);
	int offset = i;

	while (i--) {
		bit = (input >> i);
		if (bit & 1) {
			output[offset - i] = '1';
		} else {
			output[offset - i] = '0';
		}
	}
	output[offset - i] = '\0';
}

static void convert_to_dec_string(int input, char output[])
{
	char aux;
	int i = 0, j = 0;
	int sign = input;

	if (sign < 0) {
		input = -input;
	}
	
	i = 0;
	do {
		output[i++] = input % 10 + '0';
	} while ((input /= 10) > 0);

	if (sign < 0)
		output[i++] = '-';
	output[i] = '\0';

	reverse(output);
}

static void convert_to_hex_string(int input, char output[])
{
	char nibble = 0;
	unsigned int i = 0;
	unsigned int n_nibbles = ((BITS_PER_BYTE * sizeof(int)) / BITS_PER_NIBBLE);

	while (i <= n_nibbles) {
		nibble = ((input >> (4 * i)) & 0xF);
		if (nibble < 0xA)
			output[(n_nibbles - 1) - i] = (char) nibble + 0x30;
		else
			output[(n_nibbles - 1) - i] = (char) nibble + 0x37;
		i++;
	}
	output[n_nibbles] = '\0';
}

void itoa(int n, char buffer[], int radix)
{
	/*initializes the output buffer*/
	int i = 0;
	for (i = 0; i < sizeof(int) * BITS_PER_BYTE; i++)
		buffer[i] = '0';

	/*select which king of conversion must be done*/
	switch(radix) {
		case BIN_BASE:
			return convert_to_bin_string(n, buffer);
			break;
		case DEC_BASE:
			return convert_to_dec_string(n, buffer);
			break;
		case HEX_BASE:
			return convert_to_hex_string(n, buffer);
			break;
	}
}
