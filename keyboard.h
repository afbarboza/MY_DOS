#ifndef	_KEYBOARD_H_
#define	_KEYBOARD_H_

#include "vga.h"

/**
* read_keyboard - reads a keyboard using BIOS call
*/
char read_keyboard(void);

#endif	/*keyboard.h*/
