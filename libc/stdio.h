#ifndef	_STDIO_H_
#define	_STDIO_H_

#include <stdarg.h>
#include <stddef.h>
#include <itoa.h>

#define	EOF	-1

int printf(const char* __restrict format, ...);

int putchar(int c);

int puts(const char *s);

#endif	/* stdio.h */
