#ifndef	_STRING_H_
#define	_STRING_H_	1

#include <stddef.h>

/**
 * strlen - calculate the length of a string.
 *
 * @s: string which size will be calculated.
 *
 * strlen return the number of bytes in the string @s
 */
unsigned int strlen(const char *s);

/**
* strcpy - copies the string pointed to by src,
* including the terminating null byte ('\0'),
* to the buffer pointed to by dest.
* Return a pointer to the destination string dest.
*/
char *strcpy(char *dst, const char *src);

/**
* memcpy - copy memory area
*
* @src:	the source memory area.
* @dst:	the destiny memory area.
* @n:	the number of bytes to be copied from src to dst.
*
* memcpy return a pointer to @dst.
*/
void *memcpy(void *dst, const void *src, size_t n);

/**
* strcmp - compare two strings s1 and s2.
*

* strcmp return negative value, if s1 is less than s2.
* zero, if s1 is equal to s2.
* positive value, if s1 is greater than s2.
*/
int strcmp(const char *s1, const char *s2);

/**
* reverse - reverse a string pointed by @s
*/
void reverse(char s[]);

char *strcat(char *dest, char *src);

#endif	/*string.h*/
