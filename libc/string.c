#include <string.h>

#define	GCC_REAL_MODE()	__asm__ __volatile__(".code16gcc;\n\t")

unsigned int strlen(const char *s)
{
	GCC_REAL_MODE();
	char *sc = NULL;
	for (sc = s; *sc != '\0'; sc++);
	return (sc - s);
}

char *strcpy(char *dst, const char *src)
{
	GCC_REAL_MODE();
	memcpy(dst, src, strlen(src));
}

void *memcpy(void *dst, const void *src, size_t n)
{
	GCC_REAL_MODE();
	char *tmp = dst;
	const char *s = src;

	while (n--)
		*tmp++ = *s++;
	return dst;
}

int strcmp(const char *s1, const char *s2)
{
	GCC_REAL_MODE();
	unsigned char c1, c2;

	while (1) {
		c1 = *s1++;
		c2 = *s2++;
		if (c1 != c2)
			return ((c1 < c2) ? -1 : 1);
		if (!c1 || !c2)
			break;
	}
	return 0;
}

void reverse(char s[])
{
	GCC_REAL_MODE();
	char c;
	int i, j;

	for (i = 0, j = (strlen(s) - 1); i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

// dest and src _must_ be explicitly allocated via malloc or stack
// using a variable. you shall not used constant arguments to this
// function.
char *strcat(char *dest, char *src)
{
	GCC_REAL_MODE();
	char *tmp = dest;
	while (*dest) {
		dest++;
	}
	while ((*dest++  = *src++) != '\0');
	return tmp;
}
