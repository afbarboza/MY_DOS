#ifndef	_STDINT_H_
#define	_STDINT_H_

typedef	char			int8_t;
typedef unsigned char		uint8_t;
typedef	short			int16_t;
typedef	unsigned short		uint16_t;
typedef	int			int32_t;
typedef	unsigned int		uint32_t;
typedef	long long int		int64_t;
typedef	unsigned long long int	uint64_t;

typedef	uint8_t		u8;
typedef	uint16_t	u16;
typedef	uint32_t	u32;
typedef uint64_t	u64;

typedef	int8_t		s8;
typedef	int16_t		s16;
typedef	int32_t		s32;
typedef	int64_t		s64;

#endif	/* stdint.h */
