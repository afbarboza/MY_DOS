#include <stdio.h>
#include <vga.h>

int putchar(int c)
{
	terminal_writechar(c);
	return 0;
}

int puts(const char *s)
{
	if (s == NULL)
		return EOF;
	terminal_writestring(s);
}

int printf(const char* __restrict format, ...)
{
	int printed = 0;
	char curr_char;
	va_list parameters;
	va_start(parameters, format);

	while (*format != '\0') {
		if (*format < 32 && *format != '\n')
			goto ignore_char;

		if (*format == '%') {
			*format++;

			int arg;
			char *buffer;
			char tmpstr[60] = "init";
			switch(*format) {
				case 'c':
					arg = va_arg(parameters, int);
					putchar(arg);
					goto ignore_char;
					break;
				case 'd':
					arg = va_arg(parameters, int);
					itoa(arg, tmpstr, DEC_BASE);
					puts(tmpstr);
					goto ignore_char;
					break;
				case 'b':
					arg = va_arg(parameters, int);
					itoa(arg, tmpstr, BIN_BASE);
					puts(tmpstr);
					goto ignore_char;
					break;
				case 'x':
					arg = va_arg(parameters, int);
					itoa(arg, tmpstr, HEX_BASE);
					puts(tmpstr);
					goto ignore_char;
					break;
				case 's':
					buffer = va_arg(parameters, char*);
					puts(buffer);
					goto ignore_char;
					break;
				default:
					goto ignore_char;
			}
		}
		print_char:
			putchar(*format);
		ignore_char:
			*format++;
			printed++;

	}
	return printed;
}
