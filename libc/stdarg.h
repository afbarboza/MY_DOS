#ifndef	_STDARG_H_
#define	_STDARG_H_

typedef	__builtin_va_list va_list;


#undef	va_start
#define	va_start(v, l) __builtin_va_start((v), (l))

#undef	va_end
#define	va_end(v) __builtin_va_end(v)

#undef	va_arg
#define	va_arg(v, l) __builtin_va_arg(v, l)

#endif	/*stdarg.h*/
