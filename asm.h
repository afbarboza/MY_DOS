#ifndef	_ASM_H_
#define	_ASM_H_

#include <stdint.h>

struct regs {
	uint32_t	ax : 32;
	uint32_t	bx : 32;
	uint32_t	cx : 32;
	uint32_t	dx : 32;
	uint32_t	di : 32;
	uint32_t	si : 32;
} __attribute__((__packed__));

uint16_t	x86_ax;
uint16_t	x86_bx;
uint16_t	x86_cx;
uint16_t	x86_dx;
uint16_t	x86_di;
uint16_t	x86_si;

#endif	/*asm.h*/
