.section	.text
.type		_start, @function
.globl		_start		
.code16

_start:
			/* BPB: Bios Parameter Block for FAT16 */
			jmp	main_prog 	# jump instruction to boot code
OEM_name:		.byte	'D', 'O', 'S', 'I', 'X', 0, 0, 0
bytes_per_sector:	.word	512
sector_per_cluster:	.byte	1
reserved_sectors:	.word	18		# for FAT12, this value _must_ be 1 (...)
number_fats:		.byte	1		# this field always contain 2 for FAT12, FAT16, FAT32
root_entries_counter:	.word	224		#  multiplied by 32, _must_ result in even multiple of bytes_per_sector
count_sectors:		.word	2880		# count of sectors in the volume = ((72 sec/track) * 80 tracks * 2 sides)
bpb_media:		.byte	0xF0		# for hard disk, value shall be 0xF0
fat_size:		.word	6		# count of sectors occupied by ONE fat
sectors_per_track:	.word	18		# for BIOS interrupt 0x13
number_heads:		.word	1		# number of heads for interrupt 0x13
hidden_sectors:		.long	0		# hidden sectors preceding the partition that contains the FAT
large_sectors:		.long	0		# 32-bits total count of sectors. _must_ be 0
drive_number:		.byte	0		# int $0x13 drive number
reserved1:		.byte	0		# must be always
extend_boot_sign:	.byte	0x29		# extended boot signature
serial_number:		.long	0x12345678	# volume serial number. just some old M$ shit
volume_label:		.ascii	"NO NAME    "
filesystem_type:	.ascii	"FAT16   "


stack_top:		.word	0x500
stack_size:		.word	0x400

main_prog:
		# disable interrupts
		cli

		#set up the kernel stack
		movw	stack_top, %sp
		addw	stack_size, %sp

		# write back cache lines before any BIOS calls
		wbinvd

		# printing message
		movw	$msg_0, %si
		call	printstr

reset_floppy:
		# reset  disk
		movb	$0x00, %ah
		movb	$0x80, %dl
		int	$0x13
		jc	reset_floppy
		

		# print user message
		movw	$msg_1, %si
		call	printstr

		# where the kernel will be copied? - $0x1000 (see Low Memory Map)
		movw	$0x100, %ax	# take care with segmentation ;-)
		movw	%ax, %es
		xorw	%bx, %bx

copy_kernel:
		#copying the kernel sector now
		mov	$0x02, %ah	# read from disk
		mov	reserved_sectors, %al	# read 20 sectors (for now, must be enough)
		mov	$0x00, %ch	# read from track 1
		mov	$0x02, %cl	# reading sector 2 (starting address of kernel)
		mov	$0x00, %dh	# head number
		mov	$0x80, %dl	# drive 0
		int	$0x13		# bios call
		#jc	copy_kernel	# copying the kernel

		movw	$msg_2, %si	# printing message on the screen
		call	printstr

		# enable interrupts and jumps to the kernel code
		sti

		mov	$0x1000, %ax
		jmp	*%ax

.include	"print.s"

msg_0:		.ascii	"> Hello World of Bootloader and Kernel Land. :D\n\r\0"
msg_1:		.ascii	"> Hard Disk Reseted\n\r\0"
msg_2:		.ascii	"> Kernel loaded.\n\r\0"
msg_3:		.ascii	">  Kernel load failed. Try again. \n\r\0"

. = _start + 510
		.byte	0x55, 0xAA
