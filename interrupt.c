#include <interrupt.h>
#include <vga.h>
#include <string.h>
#include <asm.h>
#include <stdio.h>
#include <keyboard.h>
#include <isr.h>

__asm__(".code16gcc\t\n");

int i = 0;

__attribute__((interrupt))
volatile void handle_interrupt()
{
	puts("Intettupt 0x0 caught\n");
}

__attribute__((interrupt))
volatile void handle_clock_tick()
{
	asm("pusha; pushf;");
	puts("tick\n");
	asm("popf; popa; iret;");
}

static void save_old_ivt(void)
{
	/* defines the address of the old ivt handelrs */
	uint32_t *old_handler_ptr = (uint32_t *) (4 * 0x1C);
	old_8254_handler = 0;

	/* store this pointer to a function in a global variable */
	old_8254_handler = *(old_handler_ptr);
}

asm(".code16gcc");
void inc_tick_counter(void)
{
asm(".code16gcc");
	i++;
	if (i == 10) {
		/*TODO: implement context switiching */
		puts("_tick_");
		i = 0;
	}
	return;
}

asm(".code16gcc");
void replace_bios_interrupt(void)
{
asm(".code16gcc");
	int handler_addr = ((int)(&handle_interrupt));
	int tick_addr = ((int)(&handle_8254));

	/* stores the old IVT table */
	save_old_ivt();

	/* install kernel 'syscalls' used in traps */
	__asm__ __volatile__
	(
		"cli;"
		"pusha;"
		"movw	$0x00, %%ax;"
		"movw	%%ax,  %%es;"
		"movw	$0x21, %%di;"
		"imul	$0x04, %%di;"
		"movw	%%bx,  %%es:(%%di);"
		"addw	$0x02, %%di;"
		"movw	$0x0000, %%es:(%%di);"
		"popa;"
		"sti;"
		:
		:"b"(handler_addr)
	);

	/* install timer interrupt handler */
	__asm__ __volatile__
	(
		"cli;"
		"pusha;"
		"movw	$0x00, %%ax;"
		"movw	%%ax,  %%es;"
		"movw	$0x1c, %%di;"
		"imul	$0x04, %%di;"
		"movw	%%bx,  %%es:(%%di);"
		"addw	$0x02, %%di;"
		"movw	$0x0000, %%es:(%%di);"
		"popa;"
		"sti;"
		:
		:"b"(tick_addr)
	);

	puts("Kernel Interrupt Table Installed\n");
}
