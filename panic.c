#include <panic.h>

__attribute__((noreturn))
volatile void panic(const char *s)
{
	printf("kernel panic: %s\n", s);
	asm("hlt;");
	abort();
}
