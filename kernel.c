#ifndef	_REAL_MODE_
#define	_REAL_MODE_
__asm__(".code16gcc\t\n");
__asm__(".globl	main_kernel\t\n");
__asm__("jmpl	$0, $main_kernel\t\n");
#endif

#include <vga.h>
#include <keyboard.h>
#include <interrupt.h>
#include <stdio.h>
#include <asm.h>
#include <memory.h>
#include <isr.h>

__attribute__((noreturn))
void main_kernel(void)
{
	/* initialize console early as possible */
	terminal_initialize();

	/* install kernel 'syscalls' */
	replace_bios_interrupt();
	asm("int $0x21;");

	/* initialize memory manager */
	mm_init();

	scrdown();

	char *ascii_table = (char *) kalloc(10 * sizeof(char));
	printf("alloc: %d\n", (uint32_t) ascii_table);
	kfree(ascii_table);

	uint32_t *ptr = (uint32_t *) kalloc(20 * sizeof(uint32_t));
	printf("alloc: %d\n", (uint32_t) ptr);
	kfree(ptr);

	asm("int $0x21;");

	/* some crazy GCC shit */
	asm("hlt;");
	while(1);
}
