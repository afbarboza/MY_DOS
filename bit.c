#include <x86.h>

uint8_t bit_test(uint8_t bitbase, uint8_t offset)
{
	uint8_t retval = 0;

	asm volatile
	(
		"xorw %%dx, %%dx;"
		"bt %%bx, %%ax;"
		"adc $0, %%dx;"
		:"=d"(retval)
		:"a"(bitbase), "b"(offset)
	);

	return retval;
}

uint8_t bit_set(uint8_t bitbase, uint8_t offset)
{
	asm volatile
	(
		"bts %%bx, %%ax;"
		:"=a"(bitbase)
		:"a"(bitbase), "b"(offset)
	);

	return bitbase;
}

uint8_t bit_clear(uint8_t bitbase, uint8_t offset)
{
	asm volatile
	(
		"btr %%bx, %%ax;"
		:"=a"(bitbase)
		:"a"(bitbase), "b"(offset)
	);
	return bitbase;
}
