#ifndef	_PANIC_H_
#define	_PANIC_H_

#include <stdio.h>

#define	abort()	while(1)

__attribute__((noreturn))
volatile void panic(const char *s);

#endif	/*panic.h*/
