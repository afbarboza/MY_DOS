#include <vga.h>
#include <string.h>
#include <interrupt.h>
#include <stdint.h>
#include <asm.h>
#include <stdio.h>
#include <keyboard.h>

static uint8_t vga_row;
static uint8_t vga_column;

void terminal_position(char row, char column)
{
	vga_row = row & 0xFF;
	vga_column = column & 0xFF;

	uint16_t flag = ((vga_row << 8) | vga_column);
	asm volatile
	(
		"movb	$0x00, %%al;\n\t"
		"movb	$0x02, %%ah;\n\t"
		"movb	$0x00, %%bh;\n\t"
		"int	$0x10;\n\t"
		:
		:"d"(flag)
	);	
}

void terminal_initialize(void)
{
	struct regs param = {
		.ax = 3,
		.bx = -1,
		.cx = -1,
		.dx = -1,
		.di = -1,
		.si = -1
	};

	make_bios_call("int $0x10;", param);

	int i = 0;
	terminal_position(0, 0);
	while (i <= (80*25)) {
		terminal_writechar(' ');
		i++;
	}
	terminal_position(0, 0);
	puts("VGA Driver Installed\n");
}

void vgaclr(void)
{
	register int i = 0;
	terminal_position(0, 0);
	while (i <= (80*25)) {
		terminal_writechar(' ');
		i++;
	}
	terminal_position(0, 0);
}

int terminal_writestring(const char *data)
{
	int retval = 0;
	while (*data != '\0') {
		char c = *data;
		terminal_writechar(c);
		data++;
		retval++;
	}
	return retval;
}

void terminal_writechar(const char c)
{
	
	if (c == '\n') {
		terminal_position(vga_row + 1, vga_column);
		return;
	}

	if (c == '\t') {
		terminal_position(vga_row, vga_column+4);
		return;
	}

	asm volatile
	(
		"movb	$0x0E, %%ah;\n\t"
		"int	$0x10;\n\t"
		:
		:"a"(c)
	);
}

void scrdown(void)
{
	read_keyboard();
	vgaclr();
}
