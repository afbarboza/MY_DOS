CC	:= gcc-6
PWD	:= $(shell pwd)
CFLAGS	:= -m16 -ffreestanding -nostdlib -nostdinc -Wunreachable-code -I$(PWD)/libc -I$(PWD)
VPATH	:= ./libc
#OBJS	:= $(find bin/*.o ! -name 'kernel.o') # kernel.o needs to be the first argument to the loader
OBJS	:= bin/vga.o bin/string.o bin/keyboard.o bin/interrupt.o bin/itoa.o bin/stdio.o bin/panic.o bin/memory.o bin/io.o bin/bit.o bin/isr.o

all:	bin/boot/bootloader.bin bin/kernel.bin

bin/kernel.bin: bin/kernel.elf
	@objcopy -O binary bin/kernel.elf bin/kernel.bin;
	@rm ./*.o;

bin/kernel.elf: bin/kernel.o $(OBJS)
	@ld -r -melf_i386 bin/kernel.o $(OBJS) -o bin/main.o;
	@ld -melf_i386 -Tlinker.ld -nostdlib --nmagic -o bin/kernel.elf bin/main.o

bin/kernel.o: kernel.c
	@$(CC) $(CFLAGS) -c kernel.c;
	@cp ./kernel.o ./bin/
	@echo "kernel.c:\t\t\tcompilado";

bin/vga.o: vga.c
	@$(CC) $(CFLAGS) -c vga.c;
	@cp ./vga.o ./bin/;
	@echo "vga.c:\t\t\t\tcompilado";

bin/panic.o: panic.c
	@$(CC) $(CFLAGS) -c panic.c;
	@cp ./panic.o ./bin/;
	@echo "panic.c:\t\t\tcompilado";

bin/string.o: string.c
	@$(CC) $(CFLAGS) -c libc/string.c;
	@cp string.o bin/string.o
	@echo "string.c:\t\t\tcompilado";

bin/stdio.o: stdio.c
	@$(CC) $(CFLAGS) -c libc/stdio.c;
	@cp stdio.o bin/stdio.o
	@echo "stdio.c:\t\t\tcompilado";

bin/keyboard.o:
	@$(CC) $(CFLAGS) -c keyboard.c;
	@cp keyboard.o bin/keyboard.o
	@echo "keyboard.c:\t\t\tcompilado";

bin/interrupt.o:
	@$(CC) $(CFLAGS) -c interrupt.c;
	@cp interrupt.o bin/interrupt.o
	@echo "interrupt.c:\t\t\tcompilado";

bin/itoa.o:
	@$(CC) $(CFLAGS) -c itoa.c;
	@cp itoa.o bin/itoa.o
	@echo "itoa.c\t\t\t\tcompilado";

bin/memory.o:
	@$(CC) $(CFLAGS) -c memory.c;
	@cp memory.o bin/memory.o;
	@echo "memory.c\t\t\t\tcompilado";

bin/io.o:
	@$(CC) $(CFLAGS) -c io.c;
	@cp io.o bin/io.o;
	@echo "io.c\t\t\t\tcompilado";

bin/bit.o:
	@$(CC) $(CFLAGS) -c bit.c;
	@cp bit.o bin/bit.o;
	@echo "bit.o\t\t\t\tcompilado";

bin/isr.o:
	@as --32 isr.S -o bin/isr.o;
	@echo "isr.o\t\t\tcompilado";

bin/boot/bootloader.bin: bin/bootloader.elf
	@objcopy -O binary bin/boot/bootloader.elf bin/boot/bootloader.bin

bin/bootloader.elf: bin/bootloader.o
	@ld -m elf_i386 -nostdlib -N -Ttext 7C00 bin/boot/bootloader.o -o bin/boot/bootloader.elf

bin/bootloader.o: bootloader.s
	@as --32 bootloader.s -o bin/boot/bootloader.o

install:
	@objdump -D ./bin/kernel.elf > ./bin/kernel.asm
	@dd if=bin/boot/bootloader.bin of=bin/floppy.flp bs=512
	@dd if=bin/kernel.bin of=bin/floppy.flp seek=1 count=17 bs=512
	@echo "inicializando emulação";
	@qemu-system-i386 -monitor stdio -hda ./bin/floppy.flp

clean:
	$(shell find . -type f -name '*.o' -exec rm {} +)
	$(shell find . -type f -name '*.bin' -exec rm {} +)
	$(shell find . -type f -name '*.iso' -exec rm {} +)
	$(shell find . -type f -name '*.elf' -exec rm {} +)
	$(shell find . -type f -name '*.flp' -exec rm {} +)
	$(shell find . -type f -name '*.asm' -exec rm {} +)

erase:
	$(shell find . -type f -name '*.o' -exec rm {} +)
	$(shell find . -type f -name '*.bin' -exec rm {} +)
	$(shell find . -type f -name '*.iso' -exec rm {} +)
	$(shell find . -type f -name '*.elf' -exec rm {} +)

git:
	git add *.s *.S *.c *.h Makefile *.ld ./libc/*.c ./libc/*.h
