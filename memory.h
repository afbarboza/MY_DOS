#ifndef	_MEMORY_H_
#define	_MEMORY_H_

#include <stdint.h>

#define	MEM_RECLAIMABLE			0x01
#define	MEM_RESERVED			0x02
#define	MEM_ACPI_RECLAIMABLE		0X03
#define	MEM_ACPI_NVS			0x04
#define	MEM_BAD				0x05

/*size of each blockmem pointed by memorymap_t */
#define	BITMAP_HOLE_SIZE		0x400

/*number of blockmems per entry - size of char in bits */
#define	BITMAP_ENTRY_SIZE		0x008

#define	BITMAP_ENOMEM			-1

#define	BITMAP_ARENA			heap_memory[0].base

#define	wbinvd()			asm("wbinvd;")

typedef struct smap_entry {
	uint32_t	base_l;
	uint32_t	base_h;
	uint32_t	length_l;
	uint32_t	length_h;
	uint32_t	type;
	uint32_t	acpi;
} __attribute__((packed)) smap_entry_t;


/**
 * mem_pool_entry: each usable memory region in upper memory ( > 1 MiB)
 *
 * @base: the start addres of available region
 * @length: the size of the region
 *
 */
struct mem_pool_entry {
	uint32_t base;
	uint32_t length;
} __attribute__((packed));

/**
 * memorymap_t: bitmap data strcuture
 *
 * @bitmap_vector: start address of bitmap vector (the bitstring)
 * @bitmap_size: number of entries at bitmap_vector
 *		(e.g. number of bytes occupied by bitmap)
 *
 */
typedef struct {
	uint8_t		*bitmap_vector;
	uint32_t	bitmap_size;
} memorymap_t;


/**
 * detect upper memory and try to found the available mem regions 
 */
void detect_upper_memory(void);

/**
 * mm_init: initalizes the memory manager.
 *
 */
void mm_init(void);

/**
 * kalloc - allocate dinamic @size bytes memory and
 *		returns a pointer to the allocated memory.
 *
 * @size: number of bytes to be allocated.
 */
void *kalloc(uint32_t size);

/**
 * kfree - frees the memory space pointed to by @ptr
 *
 * @ptr: address of the memory to be released.
 *
 */
void kfree(void *ptr);

#endif	/* memory.h */
