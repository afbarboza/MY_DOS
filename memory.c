#include <memory.h>
#include <interrupt.h>
#include <asm.h>
#include <stdio.h>
#include <panic.h>
#include <stddef.h>
#include <x86.h>

__asm__(".code16gcc\n");

/* start address of list of empty memory holes, coming from BIOS */
#define	HEAP_AVAILABLE_AREA	0x8C00

/* bitmap data structure address */
#define	BITMAP_ADDRESS		0x8D00

/* start addres of bitvector of available memory areas */
#define	BITMAP_AREA		0x8F00

/* where the responses to the BIOS will be put? 0x9C00 */
#define	SMAP_BUFFER_ADDR	0x9C00
#define SMAP_BUFFER_SEG		0x9C0

/**
 * mem_bits: bitmap of available memory at high memory ( > 1MiB)
 *
 */
memorymap_t *bitmap = NULL;

/**
 * smap: list of _all_ memory regions
 */
smap_entry_t *smap = NULL;

/**
 * heap of _all_ available and reclaimable memory
 * (igoring ACPI reclaimable memory)
 */
struct mem_pool_entry *heap_memory = (struct mem_pool_entry *) HEAP_AVAILABLE_AREA;

/* total amount of memory regions that BIOS allow us to use */
uint8_t heap_pool_size = 0;

static int __attribute__((noinline, regparm(3))) detect_mem(smap_entry_t* buffer)
{
	/* request from BIOS the amount of upper available memory */
	int n_entries = 0;
	load_es(SMAP_BUFFER_SEG);
	struct regs param = {
		.ax = 0xE820,
		.bx = 0x0,
		.cx = 0x18,
		.dx = 0x534D4150,
		.di = 0,
		.si = 0
	};
	make_bios_call("int $0x15;", param);

	/*ask for each available region in upper memory */
	while (param.bx) {
		if (param.ax != 0x534D4150) {
			panic("memory detection failed\n");
		}

		param.ax = 0xE820;
		param.cx = 0x18;
		param.di += 24;
		n_entries++;
		buffer++;
		make_bios_call("int $0x15;", param);
	}

	/*returns the total amount of available memory */
	return n_entries;
}

static void __attribute__((fastcall)) heapify_pool_mem(uint8_t idx)
{
	uint32_t tmp_base = (smap[idx].base_h + smap[idx].base_l);
	uint32_t tmp_length = (smap[idx].length_h + smap[idx].length_l);

	/* we shall avoid use low memory (< 0xFFFFF) */
	if ((tmp_base + tmp_length) < 0xFFFFF) {
		return;
	}

	/* updating the new pool of memory fields */
	heap_memory->base = tmp_base;
	heap_memory->length = tmp_length;

	/* adjust number of available pools and pointer of heap*/
	heap_memory++;
	heap_pool_size++;
}

void detect_upper_memory(void)
{
	int i = 0;

	/*step 1: we shall query BIOS for all mem regions - usable or not */
	smap = (smap_entry_t *) SMAP_BUFFER_ADDR;
	puts("memory map: \n");
	int entry_count = detect_mem(smap);

	/*step 2: check each region, finding the available ones */
	smap = (smap_entry_t *) SMAP_BUFFER_ADDR;
	for (i = 0; i < entry_count; i++) {

		printf("0x%x%x - 0x%x%x: ", smap[i].base_h,   smap[i].base_l,
					    smap[i].length_h, smap[i].length_l);

		switch (smap[i].type) {
			case MEM_RECLAIMABLE:
				heapify_pool_mem(i);
				puts("free\n");
				break;
			case MEM_RESERVED:
				puts("reserved\n");
				break;
			case MEM_ACPI_RECLAIMABLE:
				puts("ACPI reclaimable\n");
				break;
			case MEM_ACPI_NVS:
				puts("ACPI NVS\n");
				break;
			case MEM_BAD:
				puts("Bad Memory Area\n");
				break;
			default:
				panic("Bad BIOS Memory Code");	
		}
	}

	/*step 3: ensure that is available memory for applications and kernel */
	if (heap_pool_size == 0) {
		panic("Not available memory for kernel and process");
	}

	/*step 4: heap_memory _must_ pont to the start of emty holes list */
	heap_memory = (struct mem_pool_entry *) HEAP_AVAILABLE_AREA;
}

static void build_memmap(void)
{
	register uint32_t i = 0;

	/* initializing bitmap data structure  */
	bitmap = (memorymap_t *) BITMAP_ADDRESS;

	/* intializing bit vector */
	bitmap->bitmap_vector = (uint8_t *) BITMAP_AREA;

	/* how many bytes will be needed to represent all physical available memory? */
	bitmap->bitmap_size = heap_memory[0].length/BITMAP_HOLE_SIZE;
	bitmap->bitmap_size /= BITMAP_ENTRY_SIZE;
	printf("%d\n", bitmap->bitmap_size);
	
	/* initializing (resetting) 8 bits per iteration */
	i = bitmap->bitmap_size;
	while (i--) {
		bitmap->bitmap_vector[i] = 0;
	}
}

static uint32_t __attribute__((fastcall)) probe_bitmap(register uint32_t n_entries)
{
	int8_t j = 0;		// iterates over each bitmap entry
	uint8_t bit;		// flag indicating whther the hole is free or not
	int32_t istart; 	// stores the index of first allocated hole
	uint32_t i;		// iterates over the bitmap
	uint32_t mapstart; 	// number of already allocated bits
	register uint8_t tmpentry = 0;

	mapstart = 0;
	istart = BITMAP_ENOMEM;
	for (i = 0; i < bitmap->bitmap_size; i++) {
		tmpentry = bitmap->bitmap_vector[i];
		for (j = 7; j >= 0; j--) {
			// enough memory already found
			if (mapstart >= n_entries) {
				goto ret_istart;
			}

			// gets the j-th bit in bmp
			bit = bit_test(tmpentry, j);

			// a free bit has been found?
			if (!bit) {
				if (istart != BITMAP_ENOMEM) {
					// just continue our greedy algorithm for free mem
					mapstart++;
				} else {
					// a free block has been found. alloc it.
					mapstart = 1;
					istart = ((i * 8) + (7 - j));

					// mark the bit as used
					tmpentry = bit_set(tmpentry, j);
				}
			} else {
				// give up. memory already been used. try again.
				mapstart = 0;
				istart = BITMAP_ENOMEM;
				// deallocate the used bits
				tmpentry = bitmap->bitmap_vector[i];
			}
			bitmap->bitmap_vector[i] = tmpentry;
		}
	}

ret_istart:
	return istart;
}

/**
 *
 * alloc_mem - allocates memory from bitmap
 *
 */
static void *alloc_mem(uint32_t size)
{
	int32_t istart;
	uint32_t retval = 0;
	uint32_t entries2alloc = 0;
	uint32_t *tmp_header_size = NULL;

	/* return NULL if try to alloc 0 bytes */
	if (size == 0) {
		goto ret_addr;
	}

	/* get room for add the header size of allocated mem */
	size += sizeof(uint32_t);

	/* we must set (@size/BITMAP_HOLE_SIZE) bits in our bitmap */
	entries2alloc = (size/BITMAP_HOLE_SIZE);
	entries2alloc += ((size % BITMAP_HOLE_SIZE) > 0 ? 1 : 0);

	/* we must search @entries2alloc consecutive bits storing 0 in out bitmap */
	istart = probe_bitmap(entries2alloc);	// stores the start index of allocated bitmap hole

	/* for now, we halt the kernel if no free mem was found */
	if (istart == BITMAP_ENOMEM) {
		panic("Not available memory for kernel and process\n");
	}

	retval = (BITMAP_ARENA + (istart * BITMAP_HOLE_SIZE));

	tmp_header_size = (uint32_t *) retval;
	*tmp_header_size = (size - sizeof(uint32_t));
	retval = (void *) (((uint32_t) retval) + sizeof(uint32_t));

ret_addr:
	return ((void *) retval);
}

/**
 * free_mem: free memory from bitmap
 *
 */
static void free_mem(void *ptr)
{
	int32_t j = 0;
	uint32_t i;
	uint32_t istart = 0;		// start index of bitmap to be released
	uint32_t size = 0;		// size of the allocated memory
	uint32_t *header_size = NULL;	// stores the size of allocated memory
	uint32_t entries2free = 0;	// number of entries to be freed in bitmap
	uint32_t cluster_byte = 0;	// which byte in bitmap the istart bit belongs to
	uint8_t cluster_offset = 0;	// index 

	/* reads the number of bytes allocated */
	header_size = (uint32_t *) ((uint32_t) ptr - 4);
	size = *header_size;

	/* calculates the start bit to be cleared in bitmap */
	istart = ((((uint32_t) ptr) - BITMAP_ARENA)/BITMAP_HOLE_SIZE);

	/* now calculates the number of bits to be cleared in bitmap */
	entries2free = (size/BITMAP_HOLE_SIZE);
	entries2free += ((size % BITMAP_HOLE_SIZE) > 0 ? 1 : 0);

	/* calculates which byte in bitmap the istart bit belongs to */
	cluster_byte = (istart / 8);
	cluster_offset = 7 - (istart % 8);

	/* effectively clear the allocated bits in our bitmap */
	for (i = cluster_byte; entries2free > 0; i++) {
		if (i == cluster_byte) {
			j = cluster_offset;
		} else {
			j = 7;
		}

		while (j >= 0 && entries2free > 0) {
			bitmap->bitmap_vector[i] = bit_clear(bitmap->bitmap_vector[i], j);
			j--;
			entries2free--;
		}
	}
}

void *kalloc(uint32_t size)
{
	return alloc_mem(size);
}

void kfree(void *ptr)
{
	free_mem(ptr);
}

void mm_init(void)
{
	/* write back and invalidate cache before any BIOS calls */
	wbinvd();

	/* probe available high memory */
	detect_upper_memory();	

	/* initialize available memory map */
	build_memmap();
}
