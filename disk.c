#include <asm.h>
#include <disk.h>
#include <stdint.h>
#include <memory.h>
#include <interrupt.h>

static void disk_reset(void)
{
	asm volatile
	(
		"pusha;"
		"movw	$0x0, %%ax;"
		"movb	$0x80, %%dl;"
		"int	$0x13;"
		"popa;"
	);
}

void lba2chs(uint32_t lba, uint8_t *head, uint8_t *track, uint8_t *sector)
{
	*head   = (lba % (sectors_per_track * 2)) / sectors_per_track;
	*track  = (lba / (sectors_per_track * 2));
	*sector = (lba % sectors_per_track + 1);
}

void init_disk(void)
{
	disk_reset();

	/*TODO: read the number of sectors per track */
}

void *dread(uint32_t lba, unsigned size)
{
	u8 h = 0, t = 0, s = 0;

	if (lba < 0 || size <= 0)
		goto fail;

	void *buffer = kalloc(size * sizeof(u8));
	if (buffer == NULL)
		goto fail;

	lba2chs(lba, &h, &t, &s);

	asm volatile
	(
		
	);

fail:
	return NULL;
}

void dwrite(const void *buff, unsigned size)
{
}
