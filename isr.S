#include "isr.h"

.code16
.globl	do_nothing
.type	do_nothing, @function
do_nothing:
	pusha
	mov	$1, %ax
	mov	$2, %bx
	movw	$isr_msg1, %si
	call	printstr
	popa
	ret

.code16
.globl	handle_8254
.type	handle_8254, @function
handle_8254:
	/* stores the proccess cntext */
	cli
	pusha
	pushf

	/* call a high level handler */
	mov	$inc_tick_counter, %eax
	call	*%eax

	/* restore the process context */
	popf
	popa
	sti
	iret

.include "print.s"
isr_msg1:	.ascii "tick \n\r\0"
