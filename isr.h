#ifndef	_ISR_H_
#define	_ISR_H_

#include <stdint.h>

uint32_t old_8254_handler;

void do_nothing(void);

__attribute__((interrupt))
volatile void handle_8254(void);

void inc_tick_counter(void);

#endif	/* isr.h */
