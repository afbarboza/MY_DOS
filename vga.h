#ifndef	_VGA_H_
#define	_VGA_H_

//#include <stddef.h>

#include <stdint.h>

/**
 * terminal_initialize - sets the default colors and
 * initial screen text. (which is a bunch of white-space) 
 */
void terminal_initialize(void);

/**
 *
 * terminal_writestring - writes @data at screen.
 *
 * return the number of chars outputed.
 */
int terminal_writestring(const char *data);

/**
 *
 * terminal_writechar - write a single char @c on screen
 */
void terminal_writechar(const char c);

/**
 * vgaclr - clear the screen
 *
 */
void vgaclr(void);

/**
 * scrdown: waits untill some key is pressed and then clears screen
 */
void scrdown(void);

#endif
